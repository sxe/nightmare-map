#!/usr/bin/env perl

use strict;
use warnings;
use Getopt::Long;
Getopt::Long::Configure ("no_ignore_case");
use File::Basename;
my %__opt;
my $__script_name = basename($0); #Removes path

__options();

my $label_location = "/home/andy/daten/daten/dokumente/eigene_programme/websites/nightmare-map/img/map/labels/";
my $black_lagoon_label_location = "/home/andy/daten/daten/dokumente/eigene_programme/websites/nightmare-map/img/map_black_lagoon/labels/";

my @text_locations = (
    { "name" => "BLACK LAGOON", "loc" => [ 13576, 8825], "type" => "link", "link" => "/black-lagoon/", "fontSize" => 28 },
    { "name" => "THE WORLD OF SYLVENUS", "loc" => [15424, 16880], "type" => "major", "fontSize" => 42 },
    { "name" => "Odium", "loc" => [5400, 9580], "type" => "major", "fontSize" => 27 },
    { "name" => "Trader's Pass", "loc" => [10000, 10320], "type" => "major", "fontSize" => 27 },
    { "name" => "Geyser Plateu", "loc" => [10720, 14080], "type" => "major", "fontSize" => 27 },
    { "name" => "The Oasis", "loc" => [10976, 3036], "type" => "major", "fontSize" => 27 },
    { "name" => "Hoarfrost", "loc" => [2080, 1872], "type" => "major", "fontSize" => 27 },
    { "name" => "Savannah", "loc" => [9904, 6588], "type" => "major", "fontSize" => 27 },
    { "name" => "Tindale", "loc" => [12040, 6328], "type" => "major", "fontSize" => 27 },
    { "name" => "Corsair Beach", "loc" => [12416, 16016], "type" => "major", "fontSize" => 27 },
    { "name" => "Kimchi Village", "loc" => [10200, 17720], "type" => "major", "fontSize" => 27 },
    { "name" => "The Carnival", "loc" => [7560, 12840], "type" => "major", "fontSize" => 27 },
    { "name" => "Meteor Valley", "loc" => [6904, 15568], "type" => "major", "fontSize" => 27 },
    { "name" => "Pirate Cove", "loc" => [3360, 18816], "type" => "major", "fontSize" => 27 },
    { "name" => "Mystic Swamps", "loc" => [ 2216, 17048], "type" => "major", "fontSize" => 27 },
    { "name" => "Nightvale", "loc" => [ 960, 13832], "type" => "major", "fontSize" => 27 },
    { "name" => "Umbra Forest", "loc" => [ 3872, 13240], "type" => "major", "fontSize" => 27 },
    { "name" => "Regal Hills", "loc" => [ 4656, 8832], "type" => "major", "fontSize" => 27 },
    { "name" => "Emberwood Forest", "loc" => [ 2696, 8672], "type" => "major", "fontSize" => 27 },
    { "name" => "Embertown", "loc" => [ 720, 7352], "type" => "major", "fontSize" => 27 },
    { "name" => "Mt Kaman", "loc" => [ 1232, 5456], "type" => "major", "fontSize" => 27 },
    { "name" => "Transylvania", "loc" => [ 7040, 5416], "type" => "major", "fontSize" => 27 },
    { "name" => "Outlaw Island", "loc" => [ 6544, 1128], "type" => "major", "fontSize" => 27 },
    { "name" => "The Crypt", "loc" => [ 7481, 17802], "type" => "minor", "fontSize" => 18 },
    { "name" => "Anchient Prison", "loc" => [ 12485, 9914], "type" => "minor", "fontSize" => 18 },
    { "name" => "Wizard Tower", "loc" => [ 9356, 6404], "type" => "minor", "fontSize" => 18 },
    { "name" => "Eternal Temple", "loc" => [ 5976, 5572], "type" => "minor", "fontSize" => 18 },
    { "name" => "Haunter House", "loc" => [ 6292, 13311], "type" => "minor", "fontSize" => 18 },
    { "name" => "Umbra Mines", "loc" => [ 5787, 13096], "type" => "minor", "fontSize" => 18 },
    { "name" => "Ice Pyramid", "loc" => [ 2746, 1400], "type" => "minor", "fontSize" => 18 },
    { "name" => "Frost Castle", "loc" => [ 3345, 1423], "type" => "minor", "fontSize" => 18 },
    { "name" => "Ice Cave", "loc" => [ 3626, 3468], "type" => "minor", "fontSize" => 18 },
    { "name" => "Frista", "loc" => [ 4113, 1824], "type" => "minor", "fontSize" => 18 },
    { "name" => "Noctunal Fellowship", "loc" => [ 4614, 3257], "type" => "minor", "fontSize" => 18 },
    { "name" => "Inferno Blood Pact", "loc" => [ 9956, 3298], "type" => "minor", "fontSize" => 18 },
    { "name" => "Orc Stronghold", "loc" => [ 10556, 4344], "type" => "minor", "fontSize" => 18 },
    { "name" => "Fire Pyramid", "loc" => [ 11879, 3349], "type" => "minor", "fontSize" => 18 },
    { "name" => "Fire Cave", "loc" => [ 12304, 2630], "type" => "minor", "fontSize" => 18 },
    { "name" => "Enchanted Castle", "loc" => [ 11014, 6256], "type" => "minor", "fontSize" => 18 },
    { "name" => "Dancing Cave", "loc" => [ 11551, 5623], "type" => "minor", "fontSize" => 18 },
    { "name" => "Tindale Mines", "loc" => [ 12807, 6223], "type" => "minor", "fontSize" => 18 },
    { "name" => "Jelly Cave", "loc" => [ 7501, 7365], "type" => "minor", "fontSize" => 18 },
    { "name" => "Savannah Mines", "loc" => [ 9368, 9673], "type" => "minor", "fontSize" => 18 },
    { "name" => "Fallen Cavern", "loc" => [ 11396, 8570], "type" => "minor", "fontSize" => 18 },
    { "name" => "Elven Labyrinth", "loc" => [ 11823, 9526], "type" => "minor", "fontSize" => 18 },
    { "name" => "The Sewers", "loc" => [ 6935, 8921], "type" => "minor", "fontSize" => 18 },
    { "name" => "Odium Castle", "loc" => [ 5928, 9580], "type" => "minor", "fontSize" => 18 },
    { "name" => "Regal Hills Mine", "loc" => [ 3615, 9373], "type" => "minor", "fontSize" => 18 },
    { "name" => "The Basement", "loc" => [ 2008, 7467], "type" => "minor", "fontSize" => 18 },
    { "name" => "Emberwood Monastery", "loc" => [ 1578, 9594], "type" => "minor", "fontSize" => 18 },
    { "name" => "Ancient Grotto", "loc" => [ 385, 8632], "type" => "minor", "fontSize" => 18 },
    { "name" => "The Occultum", "loc" => [ 2308, 12049], "type" => "minor", "fontSize" => 18 },
    { "name" => "Earth Pyramid", "loc" => [ 3038, 14759], "type" => "minor", "fontSize" => 18 },
    { "name" => "Full Moon Tribe", "loc" => [ 4295, 15194], "type" => "minor", "fontSize" => 18 },
    { "name" => "Earth Cave", "loc" => [ 4588, 13548], "type" => "minor", "fontSize" => 18 },
    { "name" => "Battle Arena", "loc" => [ 6609, 11568], "type" => "minor", "fontSize" => 18 },
    { "name" => "Guard Tower", "loc" => [ 6053, 16959], "type" => "minor", "fontSize" => 18 },
    { "name" => "MV Cave", "loc" => [ 7716, 15488], "type" => "minor", "fontSize" => 18 },
    { "name" => "Orc Fortress", "loc" => [ 8389, 13531], "type" => "minor", "fontSize" => 18 },
    { "name" => "Storm Castle", "loc" => [ 9181, 14810], "type" => "minor", "fontSize" => 18 },
    { "name" => "Thunder Cave", "loc" => [ 9750, 15681], "type" => "minor", "fontSize" => 18 },
    { "name" => "Thunder Pyramid", "loc" => [ 11206, 15879], "type" => "minor", "fontSize" => 18 },
    { "name" => "Shadow Coalition", "loc" => [ 11879, 13265], "type" => "minor", "fontSize" => 18 },
    { "name" => "Marauder's Cave", "loc" => [ 11751, 17966], "type" => "minor", "fontSize" => 18 },
    { "name" => "Kimchi Underground", "loc" => [ 10833, 17900], "type" => "minor", "fontSize" => 18 },
    { "name" => "Shore Cavern", "loc" => [ 1278, 17695], "type" => "minor", "fontSize" => 18 },
    { "name" => "Cursed Estate", "loc" => [ 1329, 14923], "type" => "minor", "fontSize" => 18 },
    { "name" => "Hysteria Hotel", "loc" => [ 396, 13616], "type" => "minor", "fontSize" => 18 },

);

my @black_lagonn_text_locations = (
    { "name" => "SYLVENUS", "loc" => [ 3992, 4550], "type" => "link", "link" => "../", "fontSize" => 28 },
    { "name" => "BLACK LAGOON", "loc" => [ 4952, 5192], "type" => "major", "fontSize" => 42 },
    { "name" => "Crucible", "loc" => [ 3592, 2608], "type" => "major", "fontSize" => 27 },
    { "name" => "Sanguis Castle", "loc" => [ 920, 912], "type" => "minor", "fontSize" => 20 },
    { "name" => "Solum Castle", "loc" => [ 1624, 3600], "type" => "minor", "fontSize" => 20 },
    { "name" => "Northwest Lighthouse", "loc" => [ 2840, 768], "type" => "minor", "fontSize" => 20 },
    { "name" => "Southwest Lighthouse", "loc" => [ 4608, 696], "type" => "minor", "fontSize" => 20 },
    { "name" => "Crucible Docks", "loc" => [ 5248, 2468], "type" => "minor", "fontSize" => 20 },
    { "name" => "East Lighthouse", "loc" => [ 2724, 4728], "type" => "minor", "fontSize" => 20 },
    { "name" => "Exile Gorge", "loc" => [ 2968, 3544], "type" => "minor", "fontSize" => 20 },

);


print "// Generated by '$__script_name'.\n";
foreach my $x (@text_locations) {
	my $name = %{$x}{"name"};
	my @loc = %{$x}{"loc"};
	my $fontSize = %{$x}{"fontSize"};
    my $type = %{$x}{"type"};
    my $link = %{$x}{"link"};

    my $fontColor = "#fff";
    if ($type eq "minor") {
        $fontColor = "#d4d4d4";
    } elsif ( $type eq "link") {
        $fontColor = "#e5c254";
    }

    if ( $__opt{"skip-main"} == 1 ) {
        next;
    }

    if ( (-f "${label_location}${name}.png") && ($__opt{"overwrite"} == 0) ) {
        next;
    }
	# { "name": "Odium", "loc": [5400, 9580], "imgSize": [123, 23], "fontSize": 2.5 }
	# -strokewidth 0.5 -stroke black

	`convert -background none -fill black -font Pixellari -pointsize ${fontSize} label:"${name}" -trim \\\( -fill "${fontColor}" label:"${name}" -set page -2-2 \\\) \\\( +clone -background black -shadow 100x3+3+3 \\\) +swap -background none -layers merge +repage "${label_location}${name}.png"`;

	my $sizeh = `identify -format "%h" "${label_location}${name}.png"`;
	my $sizew = `identify -format "%w" "${label_location}${name}.png"`;

    my $linkText = "";
    if ($link) {
        $linkText = '"link": "' . $link . '", ';
    }

	print '{ "name": "' . $name . '", "loc": [' . $loc[1][0] . ', ' . $loc[1][1] . '], "imgSize": ['. $sizew . ', ' . $sizeh .'], "type": "' . $type . '", ' . $linkText . '"fontSize": ' . $fontSize . ' },' . "\n";

}

foreach my $x (@black_lagonn_text_locations) {
    my $name = %{$x}{"name"};
    my @loc = %{$x}{"loc"};
    my $fontSize = %{$x}{"fontSize"};
    my $type = %{$x}{"type"};
    my $link = %{$x}{"link"};

    my $fontColor = "#fff";
    if ($type eq "minor") {
        $fontColor = "#d4d4d4";
    } elsif ( $type eq "link") {
        $fontColor = "#e5c254";
    }

    if ( $__opt{"skip-bl"} == 1 ) {
        next;
    }

    if ( (-f "${black_lagoon_label_location}${name}.png") && ($__opt{"overwrite"} == 0) ) {
        next;
    }

    # { "name": "Odium", "loc": [5400, 9580], "imgSize": [123, 23], "fontSize": 2.5 }
    # -strokewidth 0.5 -stroke black
    `convert -background none -fill black -font Pixellari -pointsize ${fontSize} label:"${name}" -trim \\\( -fill "${fontColor}" label:"${name}" -set page -2-2 \\\) \\\( +clone -background black -shadow 100x3+3+3 \\\) +swap -background none -layers merge +repage "${black_lagoon_label_location}${name}.png"`;

    my $sizeh = `identify -format "%h" "${black_lagoon_label_location}${name}.png"`;
    my $sizew = `identify -format "%w" "${black_lagoon_label_location}${name}.png"`;

    my $linkText = "";
    if ($link) {
        $linkText = '"link": "' . $link . '", ';
    }

    print '{ "name": "' . $name . '", "loc": [' . $loc[1][0] . ', ' . $loc[1][1] . '], "imgSize": ['. $sizew . ', ' . $sizeh .'], "type": "' . $type . '", ' . $linkText . '"fontSize": ' . $fontSize . ' },' . "\n";
}



# Command line options processing
sub __options {

    # setup defaults
    %__opt=(
        'help' => 0,
        'overwrite' => 0,
        'skip-main' => 0,
        'skip-bl' => 0,
        'skip-links' => 0,
        'verbose' => 0

    );

    GetOptions(\%__opt,
        'help|?|h',
        'overwrite|o',
        'skip-main|sm',
        'skip-bl|sbl',
        'skip-links|sl',                
        'verbose|v'

    ) or die ("\n Could not parse parameters!\n");

    __usage() if $__opt{help};

}

# Message about this program and how to use it
sub __usage {
print STDERR << "EOF";

    $__script_name helps you move and organize your media.

    usage: $__script_name [--option] [file] [directory]

    --overwrite, -o     : Ignore existing files and generate everything again.   
    --skip-main, -sm    : Ignore existing files and generate everything again.   
    --skip-bl, -sbl     : Ignore existing files and generate everything again.   
    --skip-links, -sl   : Ignore existing files and generate everything again.   

    --help, -h          : This (help) message



EOF

exit 1;

}