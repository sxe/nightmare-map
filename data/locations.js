
 
/**
 * All location data that will be added to the map. 
 */
var boss_locations = [
    { "name": "Clown", "loc": [ 6828, 13367] },
    { "name": "Basher", "loc": [ 8635, 13545] },
    { "name": "Meteor Parasite", "loc": [ 7768, 15384] },
    { "name": "Poltergeist", "loc": [ 5971, 12732] },
    { "name": "Dark Cleric", "loc": [ 6257, 5579] },
    { "name": "Skar", "loc": [ 12722, 9747] },
    { "name": "Venus Flytrap", "loc": [7703, 7393] },
    { "name": "Dracula", "loc": [ 7146, 5710] },
    { "name": "General Kain", "loc": [ 6368, 16976] },
    { "name": "Angry Chef", "loc": [6144, 9581] },
    { "name": "Stone Golem", "loc": [ 3926, 9498] },
    { "name": "Black Widow", "loc": [ 9644, 8478] },
    { "name": "King Jelli", "loc": [ 7155, 8922] },
    { "name": "Chief Mirebog", "loc": [13163, 8927] },
    { "name": "Warlock Zenith", "loc": [ 11297, 6256] },
    { "name": "Imp King", "loc": [ 11755, 5629] },
    { "name": "Thrasher", "loc": [ 10957, 4333] },
    { "name": "Captain Redbeard", "loc": [ 4209, 18044] },
    { "name": "Yeti", "loc": [ 3425, 3513] },
    { "name": "Prince Sylven VI", "loc": [ 3643, 1423] },
    { "name": "Cyclops", "loc": [ 10272, 15680] },
    { "name": "Ninja Genji", "loc": [ 10576, 17704] },
    { "name": "Minotaur", "loc": [ 4365, 12731] },
    { "name": "Ifrit", "loc": [ 12507, 2625] },
    { "name": "Instructor Orpheus", "loc": [ 12196, 6314] },
    { "name": "Lord Darkoth", "loc": [ 1557, 14763] },
    { "name": "Goblin King", "loc": [ 1547, 17696] },
    { "name": "Necromancer", "loc": [ 7712, 17808] },
    { "name": "Captain Rickard", "loc": [ 9520, 14824] },
    { "name": "Master Seldar", "loc": [ 2130, 9579] },
    { "name": "Arch Priest", "loc": [ 2848, 12042] },
    { "name": "Queen Recluse", "loc": [ 1649, 7724] },
    { "name": "Archon Daikokuten", "loc": [ 2962, 14908] },
    { "name": "Archon Jupiter", "loc": [ 11090, 15893] },
    { "name": "Archon Hephaestus", "loc": [ 12089, 3360] },
    { "name": "Archon Khione", "loc": [ 3072, 1400] },

];

var fishing_locations = [
    { "name": "Fishing spot (T1, 1)", "loc": [ 8691, 10645] },
    { "name": "Fishing spot (T2, 1)", "loc": [ 10922, 8553] },
    { "name": "Fishing spot (T2, 2)", "loc": [ 4312, 8248] },
    { "name": "Fishing spot (T2, 3)", "loc": [ 7880, 12485] },
    { "name": "Fishing spot (T3, 1)", "loc": [ 5224, 16922] },
    { "name": "Fishing spot (T3, 2)", "loc": [ 7439, 6777] },
    { "name": "Fishing spot (T3, 3)", "loc": [ 7192, 2952] },
    { "name": "Fishing spot (T4, 1)", "loc": [ 11802, 14013] },
    { "name": "Fishing spot (T4, 2)", "loc": [ 2121, 6688] },
    { "name": "Fishing spot (T4, 3)", "loc": [ 3768, 3872] },
    { "name": "Fishing spot (T4, 4)", "loc": [ 972, 12555] },

];

var quest_locations = [
    { "name": "Woodcutting Quests (Daily)", "loc": [ 8432, 7979] },
    { "name": "Mining Quests (Daily)", "loc": [ 7660, 11646] },
    { "name": "Fishing Competition (Daily)", "loc": [ 7097, 7033] },
    { "name": "Wisdom of Fools", "loc": [ 5243, 8158] },
    { "name": "Cleaning up the town", "loc": [ 6963, 8285] },
    { "name": "Slavery (lvl 12)", "loc": [ 9740, 9625] },
    { "name": "The Hunted", "loc": [ 11598, 10459] },
    { "name": "Life's A Beach", "loc": [ 12240, 18909] },
    { "name": "Wizard's Wish (Broken mirror quest)", "loc": [ 9639, 6398] },
    { "name": "Espionage", "loc": [7160, 11865] },
    { "name": "Insanity (lvl 30 )", "loc": [7669, 14810] },
    { "name": "Cleanse The Evil (lvl 25)", "loc": [7348, 17017] },
    { "name": "Strange Tongue (lvl 35)", "loc": [2325, 17374] },
    { "name": "Retirement (lvl 35, Rewards Monestary key)", "loc": [1493, 9400] },
    { "name": "Impish Deeds (lvl 30)", "loc": [11804, 7224] },
    { "name": "The One That Got Away (lvl 30, Ancient Rod)", "loc": [7908, 3388] }, 
    { "name": "Territorial Dispute", "loc": [ 7115, 7422] }, 
    { "name": "Earl the Greys Youth Potion (Daily, Stat reset potion)", "loc": [ 5782, 9296] },
    { "name": "Arachnophobia", "loc": [ 1602, 7768] },
    { "name": "Traitors (lvl 35)", "loc": [9398, 19030] },
    { "name": "25 light ale", "loc": [6265, 19451] },
    { "name": "Treasure Map (Not implemented)", "loc": [4501, 19837] },

    { "name": "The Ritual (Daily, Mid level)", "loc": [8415, 3743] },
    { "name": "Business In Kimchi (Daily, High level)", "loc": [ 9776, 17504] },
    { "name": "Business In Nightvale (Daily, High level)", "loc": [ 1850, 14824] },
    { "name": "Business In Tindale (Daily, High level)", "loc": [ 12730, 6812] },

    { "name": "Jelly Suppression Order (Daily, Low level)", "loc": [ 6252, 10000] },
    { "name": "North Suppression Order I (Daily, Mid level)", "loc": [ 6252, 10040] },
    { "name": "South Suppression Order I (Daily, High level)", "loc": [ 6252, 10080] },

];

var shop_locations = [
    { "name": "Grappling Hook (15k)", "loc": [ 12940, 8283] },
    { "name": "Traveling Merchant (Tools)", "loc": [ 7432, 8635] },
    { "name": "Novice Angler Rod Vendor (T1)", "loc": [ 8548, 10810] },
    { "name": "Angler Rod Vendor (T2, You need a Grappling Hook to reach it.)", "loc": [ 12905, 7644] },

    { "name": "Alchemy ((1-25))", "loc":    [ 7969, 9372]},
    { "name": "Rubies (1-25)", "loc":    [ 7515, 8795]},
    { "name": "Textiles (1-25)" ,"loc":    [ 7228, 9149]},
    { "name": "Grocer", "loc":    [ 6713, 9177]},
    { "name": "Metalwork (1-25)", "loc":    [ 6969, 10011]},
    { "name": "Enchanting", "loc":    [ 7512, 10266]},
    { "name": "Guardians (Pets)", "loc":    [ 7509, 10841]},
    { "name": "Leatherwork (1-25)", "loc":    [ 7961, 10843]},
    { "name": "Pub", "loc":    [ 6131, 10554]},
    { "name": "Black Market", "loc":    [ 6247, 10393]},

    { "name": "Pub", "loc": [13203, 6842]},    
    { "name": "Leatherwork", "loc": [13206, 6460]},    
    { "name": "Alchemy", "loc": [13203, 6234]},    
    { "name": "Gemcutting", "loc": [13202, 5818]},    
    { "name": "Textiles", "loc": [13298, 5402]},    
    { "name": "Gemcutting", "loc": [13298, 5402]},    
    { "name": "Grocer", "loc": [12978, 5242]},    
    { "name": "Bank", "loc": [12305, 5851]},    
    { "name": "Metalwork", "loc": [12882, 6299]},    

    { "name": "Enchant (25)", "loc": [1073, 6842]},    
    { "name": "Textiles (30)", "loc": [1073, 7418]},    
    { "name": "Leatherwork (35)", "loc": [12882, 6299]},    
    { "name": "Metalwork (35)", "loc": [1041, 8122]},    
    { "name": "Alchemy", "loc": [1424, 7995 ]},    
    { "name": "Bank", "loc": [1905, 7354]},    
    { "name": "Grocer", "loc": [1904, 7003]},    
    { "name": "Pub", "loc": [1488, 7225]},    

    { "name": "Leatherwork (35)", "loc": [11317, 17847]},    
    { "name": "Metalwork (35)", "loc": [11028, 17498]},    
    { "name": "Alchemy", "loc": [10419, 17466]},    
    { "name": "Pub", "loc": [10516, 18075]},    
    { "name": "Grocer", "loc": [10164, 18105]},    
    { "name": "Enchant", "loc": [1488, 7225]},    
    { "name": "Pub", "loc": [9392, 17113]},    

    { "name": "Textiles (35)", "loc": [2258, 14394]},    
    { "name": "Leatherwork (35)", "loc": [1973, 14298]},    
    { "name": "Gemcutting", "loc": [1906, 14074]},    
    { "name": "Enchant (35)", "loc": [1520, 14267]},    
    { "name": "Pub", "loc": [1454, 13497]},    
    { "name": "Woodcutter", "loc": [1105, 13401]},    

];

var misc_locations = [
    { "name": "Teleport (Entrance to Nightvale)", "loc": [2017, 15544 ] },
    { "name": "Ice Wand (Key Item)", "loc": [ 1820, 9576] },
    { "name": "Fire Wand (Key Item)", "loc": [ 11150, 6245] },
    { "name": "Crystal Mirror (to activate crystals)", "loc": [9526, 6392] },
    { "name": "Ice castle puzzle (Opens ice castle)", "loc": [ 4343, 1754] },
];

/**
 * Locations that are not yet fully confirmed or have the wrong text.
 */
var test_locations = [

    { "name": "Meet with Lucid to go to black lagoon", "loc": [5366, 10331] },

];

var text_locations = [
// Generated by 'create_labels.pl'.
{ "name": "BLACK LAGOON", "loc": [13676, 8825], "imgSize": [213, 41], "type": "link", "link": "./black-lagoon/", "fontSize": 28 },
{ "name": "THE WORLD OF SYLVENUS", "loc": [15424, 16880], "imgSize": [528, 56], "type": "major", "fontSize": 42 },
{ "name": "Odium", "loc": [5400, 9580], "imgSize": [88, 41], "type": "major", "fontSize": 27 },
{ "name": "Trader's Pass", "loc": [10000, 10320], "imgSize": [184, 41], "type": "major", "fontSize": 27 },
{ "name": "Geyser Plateu", "loc": [10720, 14080], "imgSize": [177, 41], "type": "major", "fontSize": 27 },
{ "name": "The Oasis", "loc": [10976, 3036], "imgSize": [133, 41], "type": "major", "fontSize": 27 },
{ "name": "Hoarfrost", "loc": [2080, 1872], "imgSize": [130, 41], "type": "major", "fontSize": 27 },
{ "name": "Savannah", "loc": [9904, 6588], "imgSize": [127, 41], "type": "major", "fontSize": 27 },
{ "name": "Tindale", "loc": [12040, 6328], "imgSize": [100, 41], "type": "major", "fontSize": 27 },
{ "name": "Corsair Beach", "loc": [12416, 16016], "imgSize": [186, 41], "type": "major", "fontSize": 27 },
{ "name": "Kimchi Village", "loc": [10200, 17720], "imgSize": [182, 41], "type": "major", "fontSize": 27 },
{ "name": "The Carnival", "loc": [7560, 12840], "imgSize": [165, 41], "type": "major", "fontSize": 27 },
{ "name": "Meteor Valley", "loc": [6904, 15568], "imgSize": [177, 41], "type": "major", "fontSize": 27 },
{ "name": "Pirate Cove", "loc": [3360, 18816], "imgSize": [150, 41], "type": "major", "fontSize": 27 },
{ "name": "Mystic Swamps", "loc": [2216, 17048], "imgSize": [197, 41], "type": "major", "fontSize": 27 },
{ "name": "Nightvale", "loc": [960, 13832], "imgSize": [125, 41], "type": "major", "fontSize": 27 },
{ "name": "Umbra Forest", "loc": [3872, 13240], "imgSize": [181, 41], "type": "major", "fontSize": 27 },
{ "name": "Regal Hills", "loc": [4656, 8832], "imgSize": [138, 41], "type": "major", "fontSize": 27 },
{ "name": "Emberwood Forest", "loc": [2696, 8672], "imgSize": [240, 41], "type": "major", "fontSize": 27 },
{ "name": "Embertown", "loc": [720, 7352], "imgSize": [147, 41], "type": "major", "fontSize": 27 },
{ "name": "Mt Kaman", "loc": [1232, 5456], "imgSize": [128, 41], "type": "major", "fontSize": 27 },
{ "name": "Transylvania", "loc": [7040, 5416], "imgSize": [165, 41], "type": "major", "fontSize": 27 },
{ "name": "Outlaw Island", "loc": [6544, 1128], "imgSize": [175, 41], "type": "major", "fontSize": 27 },
{ "name": "The Crypt", "loc": [7481, 17802], "imgSize": [94, 32], "type": "minor", "fontSize": 18 },
{ "name": "Anchient Prison", "loc": [12485, 9914], "imgSize": [137, 32], "type": "minor", "fontSize": 18 },
{ "name": "Wizard Tower", "loc": [9356, 6404], "imgSize": [126, 32], "type": "minor", "fontSize": 18 },
{ "name": "Eternal Temple", "loc": [5976, 5572], "imgSize": [133, 32], "type": "minor", "fontSize": 18 },
{ "name": "Haunter House", "loc": [6292, 13311], "imgSize": [128, 32], "type": "minor", "fontSize": 18 },
{ "name": "Umbra Mines", "loc": [5787, 13096], "imgSize": [118, 32], "type": "minor", "fontSize": 18 },
{ "name": "Ice Pyramid", "loc": [2746, 1400], "imgSize": [109, 32], "type": "minor", "fontSize": 18 },
{ "name": "Frost Castle", "loc": [3345, 1423], "imgSize": [110, 32], "type": "minor", "fontSize": 18 },
{ "name": "Ice Cave", "loc": [3626, 3468], "imgSize": [82, 32], "type": "minor", "fontSize": 18 },
{ "name": "Frista", "loc": [4113, 1824], "imgSize": [61, 32], "type": "minor", "fontSize": 18 },
{ "name": "Noctunal Fellowship", "loc": [4614, 3257], "imgSize": [171, 32], "type": "minor", "fontSize": 18 },
{ "name": "Inferno Blood Pact", "loc": [9956, 3298], "imgSize": [161, 32], "type": "minor", "fontSize": 18 },
{ "name": "Orc Stronghold", "loc": [10556, 4344], "imgSize": [137, 32], "type": "minor", "fontSize": 18 },
{ "name": "Fire Pyramid", "loc": [ 11879, 3349], "imgSize": [117, 32], "type": "minor", "fontSize": 18 },
{ "name": "Fire Cave", "loc": [12304, 2630], "imgSize": [90, 32], "type": "minor", "fontSize": 18 },
{ "name": "Enchanted Castle", "loc": [11014, 6256], "imgSize": [148, 32], "type": "minor", "fontSize": 18 },
{ "name": "Dancing Cave", "loc": [11551, 5623], "imgSize": [120, 32], "type": "minor", "fontSize": 18 },
{ "name": "Tindale Mines", "loc": [12807, 6223], "imgSize": [121, 32], "type": "minor", "fontSize": 18 },
{ "name": "Jelly Cave", "loc": [7501, 7365], "imgSize": [93, 32], "type": "minor", "fontSize": 18 },
{ "name": "Savannah Mines", "loc": [9368, 9673], "imgSize": [139, 32], "type": "minor", "fontSize": 18 },
{ "name": "Fallen Cavern", "loc": [11396, 8570], "imgSize": [121, 32], "type": "minor", "fontSize": 18 },
{ "name": "Elven Labyrinth", "loc": [11823, 9526], "imgSize": [136, 32], "type": "minor", "fontSize": 18 },
{ "name": "The Sewers", "loc": [6935, 8921], "imgSize": [110, 32], "type": "minor", "fontSize": 18 },
{ "name": "Odium Castle", "loc": [5928, 9580], "imgSize": [117, 32], "type": "minor", "fontSize": 18 },
{ "name": "Regal Hills Mine", "loc": [3615, 9373], "imgSize": [138, 32], "type": "minor", "fontSize": 18 },
{ "name": "The Basement", "loc": [2008, 7467], "imgSize": [125, 32], "type": "minor", "fontSize": 18 },
{ "name": "Emberwood Monastery", "loc": [1578, 9594], "imgSize": [195, 32], "type": "minor", "fontSize": 18 },
{ "name": "Ancient Grotto", "loc": [385, 8632], "imgSize": [126, 32], "type": "minor", "fontSize": 18 },
{ "name": "The Occultum", "loc": [2308, 12049], "imgSize": [121, 32], "type": "minor", "fontSize": 18 },
{ "name": "Earth Pyramid", "loc": [3038, 14759], "imgSize": [128, 32], "type": "minor", "fontSize": 18 },
{ "name": "Full Moon Tribe", "loc": [4295, 15194], "imgSize": [137, 32], "type": "minor", "fontSize": 18 },
{ "name": "Earth Cave", "loc": [4588, 13548], "imgSize": [101, 32], "type": "minor", "fontSize": 18 },
{ "name": "Battle Arena", "loc": [6609, 11568], "imgSize": [111, 32], "type": "minor", "fontSize": 18 },
{ "name": "Guard Tower", "loc": [6053, 16959], "imgSize": [119, 32], "type": "minor", "fontSize": 18 },
{ "name": "MV Cave", "loc": [7716, 15488], "imgSize": [83, 32], "type": "minor", "fontSize": 18 },
{ "name": "Orc Fortress", "loc": [8389, 13531], "imgSize": [119, 32], "type": "minor", "fontSize": 18 },
{ "name": "Storm Castle", "loc": [9181, 14810], "imgSize": [116, 32], "type": "minor", "fontSize": 18 },
{ "name": "Thunder Cave", "loc": [9750, 15681], "imgSize": [125, 32], "type": "minor", "fontSize": 18 },
{ "name": "Thunder Pyramid", "loc": [11206, 15879], "imgSize": [152, 32], "type": "minor", "fontSize": 18 },
{ "name": "Shadow Coalition", "loc": [11879, 13265], "imgSize": [148, 32], "type": "minor", "fontSize": 18 },
{ "name": "Marauder's Cave", "loc": [11751, 17966], "imgSize": [148, 32], "type": "minor", "fontSize": 18 },
{ "name": "Kimchi Underground", "loc": [10833, 17900], "imgSize": [178, 32], "type": "minor", "fontSize": 18 },
{ "name": "Shore Cavern", "loc": [1278, 17695], "imgSize": [124, 32], "type": "minor", "fontSize": 18 },
{ "name": "Cursed Estate", "loc": [1329, 14923], "imgSize": [125, 32], "type": "minor", "fontSize": 18 },
{ "name": "Hysteria Hotel", "loc": [396, 13616], "imgSize": [125, 32], "type": "minor", "fontSize": 18 },

];

var all_locations = [
    { name: "Major Landmarks", val: text_locations, color: "#000", type: "majorText"},
    { name: "Minor Landmarks", val: text_locations, color: "#000", type: "minorText"},
    { name: "Dungeon bosses", val: boss_locations, color: "#701e20", icon: "boss.png" },
    { name: "Fishing locations", val: fishing_locations, color: "#277388", icon: "fish.png" },
    { name: "Quest locations", val: quest_locations, color: "#9d893a", icon: "quest.png" },
    { name: "Shop locations", val: shop_locations, color: "#916947", icon: "shop.png" },
    { name: "Misc. locations", val: misc_locations, color: "#583470", icon: "misc.png" },
    { name: "Unconfirmed locations", val: test_locations, color: "#000", icon: "test.png", status: "disabled" },
];
