
var groupLocationData = function(rc, data, color, icon, dir){
    var group = L.layerGroup();

    var myIcon = L.icon({
        iconUrl: dir + '/' + icon,
        iconSize: [32, 32],
        iconAnchor: [16, 32],
        popupAnchor: [0, -16]
    });

    for(i in data) {
        // X, Y
        //var cloc = [ 17440 - (17440/5012) * data[i].loc[0] - 50, (20284/5952) * data[i].loc[1] - 200];
        //console.log('{ "name": "'+data[i].name+'", "loc": ['+Math.ceil(cloc[0])+', '+Math.ceil(cloc[1])+'] },');

        var loc = [ data[i].loc[1], data[i].loc[0] ];
        var searchName = data[i].name.replace( /\(.*?\)/g, "" ).trim();
        var marker = new L.Marker(new L.latLng(rc.unproject(loc)), {title: data[i].name, icon: myIcon} );

        marker.bindPopup("<center>" + data[i].name + '<br><a target="_blank" href="https://nightmaremmorpg.fandom.com/wiki/Special:Search?query=' + searchName + '&scope=internal&navigationSearch=true">Wiki</a></center>');
        marker.addTo(group);
    }
    return group;
}

var groupTextLocationData = function(rc, data, color, type, dir) {
    var group = L.layerGroup();



    var myIcon, markerHtmlStyles;
    for(i in data) {

        if ( (data[i].type != type) && ( data[i].type !== "link")  ) {
            continue;
        };

        // FIXME: Hacky way to prevent adding links twice.
        if ( (type == "minor") && ( data[i].type == "link")  ) {
            continue;
        };

        var myIcon = L.icon({
            iconUrl: dir + '/labels/' + data[i].name + '.png',
            iconSize: [ data[i].imgSize[0], data[i].imgSize[1] ],
            //iconAnchor: [95, 45],
            popupAnchor: [0, -16]
        });

        var loc = [ data[i].loc[1], data[i].loc[0] ];
        //console.log(name, loc)

        var options = {interactive: false, title: data[i].name, icon: myIcon/*, zIndexOffset: 1000*/};
        if (data[i].type == "link")
            options = {title: data[i].name, icon: myIcon, link: data[i].link};
        var marker = new L.Marker(new L.latLng(rc.unproject(loc)), options );
        if (data[i].type == "link")
            marker.on('click', onClick);
        marker.addTo(group);
    }
    return group;

}


function onClick(e) {
    console.log(this.options.link);
    window.location.href = this.options.link;
    //window.open(this.options.win_url);
}

var initMap = function(args){

    args = args || {};
    var minZoom = args.minZoom || 2.5;
    var maxZoom = args.maxZoom || 6.5;
    var imgDimensions = args.imgDimensions || [
      20284, // original width of image `karta.jpg`
      17440  // original height of image
    ];
    var imgDir = args.imgDir || "img";
    var mapDir = args.mapDir || "map";

    // create the map
    var map = L.map("map", {
        minZoom: minZoom,
        maxZoom: maxZoom,
        zoomControl: false,
        zoomSnap: 0.5,
        zoomDelta: 1
    })

    // assign map and image dimensions
    var rc = new L.RasterCoords(map, imgDimensions);

    /**
     * Configure and add map.
     */
    // set the view on a marker ...
    map.setView([0,0], minZoom);

    // the tile layer containing the image generated with gdal2tiles --leaflet ...
    L.tileLayer(imgDir + "/" + mapDir + '/tiles/{z}/{x}/{y}.png', {
      noWrap: true,
      attribution: "Nightmare © <a target='_blank' href='https://nightmare.mageworkstudios.com'>Magework Studios</a> | This is a fan site and in no way affiliated with Magework Studios. | <a target='_blank' href='https://gitlab.com/sxe/nightmare-map/-/issues'>Found a bug?</a>"
    }).addTo(map)
      

    /**
     * Add location data to the map.
     */
    var location_groups = [];
    for (i in all_locations) {
        if(all_locations[i].type == "majorText") {
            location_groups.push(groupTextLocationData(rc, all_locations[i].val, all_locations[i].color, "major", imgDir + "/" + mapDir));
        } else if(all_locations[i].type == "minorText") {
            location_groups.push(groupTextLocationData(rc, all_locations[i].val, all_locations[i].color, "minor", imgDir + "/" + mapDir));
        } else {
            location_groups.push(groupLocationData(rc, all_locations[i].val, all_locations[i].color, all_locations[i].icon, imgDir));
        }
    }

    var overlays = {};
    for (i in location_groups) {
        overlays["<b><span style='color: " + all_locations[i].color + "'>" + all_locations[i].name + "</span></b>"] = location_groups[i];
        if(all_locations[i].status != "disabled")
            location_groups[i].addTo(map); // add all markers to map by defautl for now
    }

    L.control.layers(null, overlays).addTo(map);


    /**
     * Adding left click coordinates
     */
    var c = L.control.coordinates({
        latitudeText: 'Y',
        longitudeText: 'X',
        promptText: 'Press Ctrl+C to copy coordinates',
        precision: 0,
        imgDimension: imgDimensions
    }).addTo(map);


    map.on('click', function (e) {
        c.setCoordinates(e);
    });


    /**
     * Adding search
     */
    function localData(text, callResponse) {

        // Combina all location data arrays that should be searched.
        // Only if currently active on the map.
        var searchable_locations = [];
        for(i in location_groups) {
            if(map.hasLayer(location_groups[i])){
                searchable_locations = searchable_locations.concat(all_locations[i].val);
            }
        }
        callResponse(searchable_locations);

        return {
            abort: function() {
                //console.log('aborted request:'+ text);
            }
        };
    }

    map.addControl( new L.Control.Search({
        sourceData: localData,
        propertyName: "name",
        zoom: 4,
        collapsed: false,
        initial: false,
        imgDimensions: imgDimensions
    }));

    L.control.zoom().addTo(map);

    var popup = L.popup().setLatLng(map.getCenter()).setContent('<p>Hi!<br />This is a fansite to make navigating around Sylvenus a bit easier.<br /><br />If you miss something on the map feel free to send a message to <strong>Andy#6348</strong> on discord.<br />Please add the full name of the item you want to be added and the coordinates where it is located on the map. You can get the coordinates by left clicking anywhere on the map and they will show on the lower left. Click the coordinates to easily copy them. Please make sure to zoom in, makes it easier to get the correct coordinates.<br /><br />Have fun!</p>');

    L.easyButton('<span class="button">?</span>', function(btn, map){
        popup.openOn(map);
    }).addTo(map);

    /**
     * Add corrdinates and zoom to the url so you can link to specific point on the map.
     */
    var hash = new L.Hash(map, imgDimensions);


    var options = { timeout: 20000, position: "bottomleft" }
    var box = L.control.messagebox(options).addTo(map);
    box.show( 'This sites content is entirely made possibel by you, the users.<br />If something is missing, please let us know by clicking the ? on the upper left. Thanks ' );    
}

if( typeof site !== 'undefined' && site == "black-lagoon" ) {
    initMap({"imgDir": "../img", "mapDir": "/map_black_lagoon", "minZoom": 2, "maxZoom": 4.5, "imgDimensions": [5120, 5824]});
} else {
    initMap({"imgDimensions": [20284, 17440]});
}